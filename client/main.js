import { Meteor } from 'meteor/meteor';
import '../imports/startup/accounts-config';
import App from '../imports/ui/entry/App';

Meteor.startup(() => {
    App.init();
});
