/**
 * @file    Config account signup method
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import { Accounts } from 'meteor/accounts-base';

Accounts.ui.config({
    passwordSignupFields: 'USERNAME_AND_EMAIL',
});
