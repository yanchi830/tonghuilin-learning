export default {
    timestamp: {
        hasCreatedField:  true,
        createdFieldName: 'createdAt',
        hasUpdatedField:  true,
        updatedFieldName: 'updatedAt',
    },
    softremove: {
        // The field name with a flag for marking a document as removed.
        removedFieldName:   'removed',
        // A flag indicating if a "removedAt" field should be present in a document.
        hasRemovedAtField:  true,
        // The field name storing the removal date.
        removedAtFieldName: 'removedAt',
    },
};
