/**
 * @file    Question collection
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import { Mongo }        from 'meteor/mongo';
import { Class, Enum }  from 'meteor/jagi:astronomy';
import Behaviors           from '../../startup/behaviors-config';

// Constants
// ---------------------------------------------
export const QuestionType = Enum.create({
    name:        'Type',
    identifiers: ['CHOICE', 'CLOZE'],
});

const COM_FIELDS = {
    title: {
        type:       String,
        validators: [
            {type: 'minLength', param: 3},
            {type: 'maxLength', param: 40},
        ],
    },
    description: String,
    type:        QuestionType,
    isActive:    Boolean,
    createdBy:   Object,
    updatedBy:   Object,
    authors:     [Object],
    behaviors:   Behaviors,
};


// Collections
// ---------------------------------------------
const ChoiceQuestions = new Mongo.Collection('choiceQuestions');
const ClozeQuestions = new Mongo.Collection('clozeQuestions');

const ChoiceQuestion = Class.create({
    name:       'ChoicesQuestion',
    collection: ChoiceQuestions,
    fields:     {
        ...COM_FIELDS,
        answers: [Object],
        options: [Object],
    },
});

const ClozeQuestion = Class.create({
    name:       'ClozeQuestion',
    collection: ClozeQuestions,
    fields:     {
        ...COM_FIELDS,
        answers: [Object],
    },
});

export default {
    ChoiceQuestions,
    ClozeQuestions,
    ChoiceQuestion,
    ClozeQuestion,
};
