/**
 * @file    User collection
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import { Meteor }        from 'meteor/meteor';
import { check }         from 'meteor/check';
import { Class, Enum }   from 'meteor/jagi:astronomy';
import Behaviors from '../../startup/behaviors-config';


// Constants
// ---------------------------------------------
export const UserType = Enum.create({
    name:        'Type',
    identifiers: ['TEACHER', 'STUDENT'],
});

export const UserProfile = Class.create({
    name:    'UserProfile',
    surname: {
        type:       String,
        default:    '',
        validators: [
            {type: 'maxLength', param: 40},
        ],
    },
    givenname: {
        type:       String,
        default:    '',
        validators: [
            {type: 'maxLength', param: 40},
        ],
    },
    type:     UserType,
    isActive: {
        type:    Boolean,
        default: true,
    },
});


const User = Class.create({
    name:       'User',
    collection: Meteor.users,
    fields:     {
        createdAt: Date,
        emails:    {
            type:    [Object],
            default: () => ([]),
        },
        profile: {
            type:    UserProfile,
            default: () => ({}),
        },
    },
    behaviors: Behaviors,
});

if (Meteor.isServer) {
    User.extend({
        fields: {
            services: Object,
        },
    });
}
