/**
 * @file    Answer Collection
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import { Mongo } from 'meteor/mongo';
import { Class, Enum } from 'meteor/jagi:astronomy';
import { QuestionType } from './question';
import Behaviors        from '../../startup/behaviors-config';

// Constants
// ---------------------------------------------
export const AnswerType = Enum.create({
    name:        'Type',
    identifiers: ['TEXT', 'AUDIO', 'VIDEO'],
});

const COM_FIELDS = {
    type:         AnswerType,
    isActive:     Boolean,
    isCorrect:    Boolean,
    question:     Object,
    questionType: QuestionType,
    createdBy:    Object,
    updatedBy:    Object,
    authors:      [Object],
    behaviors:    Behaviors,
};

// Collections
// ---------------------------------------------
const TextAnswers = Mongo.Collection('textAnswers');
const MediaAnswers = Mongo.Collection('mediaAnswers');

const TextAnswer = Class.create({
    name:       'TextAnswer',
    collection: TextAnswers,
    fields:     {
        ...COM_FIELDS,
        value: {
            type:       String,
            validators: [
                {type: 'minLength', param: 1},
                {type: 'maxLength', param: 100},
            ],
        },
    },
});

const MediaAnswer = Class.create({
    name:       'MediaAnswer',
    collection: MediaAnswers,
    fields:     {
        ...COM_FIELDS,
        value: Object,
    },
});

TextAnswer.save();
MediaAnswer.save();

export default {
    TextAnswers,
    MediaAnswers,
    TextAnswer,
    MediaAnswer,
};

