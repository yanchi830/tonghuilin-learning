import 'bootstrap/dist/css/bootstrap.css';
import React                    from 'react';
import { render }               from 'react-dom';
import { Provider }             from 'react-redux';
import {
    createStore, combineReducers,
}                               from 'redux';
import {
    Router, Route, Switch,
}                               from 'react-router';
import createBrowserHistory     from 'history/createBrowserHistory';
import { assign }               from 'lodash/fp';

import AuthContainer            from '../account/auth/AuthContainer';
import LoginContainer           from '../account/login/LoginContainer';
import SignUpContainer          from '../account/signup/SignUpContainer';
import UpdateProfileContainer   from '../account/update-profile/UpdateProfileContainer';
import ProfileContainer         from '../account/profile/ProfileContainer';


// Constants
// ------------------------------------------------------
const w = typeof window === 'undefined' ? {} : window;

// Create store
// ------------------------------------------------------
const reducers = [
    AuthContainer,
    LoginContainer,
    SignUpContainer,
    UpdateProfileContainer,
    ProfileContainer,
].reduce((acc, container) => assign(acc, container.reducer), {});

const store = createStore(
    combineReducers(reducers),
    w.__REDUX_DEVTOOLS_EXTENSION__ && w.__REDUX_DEVTOOLS_EXTENSION__(),
);

// Router history
// ------------------------------------------------------
const history = createBrowserHistory();

const App = () => {
    let auth = {
        initialised: false,
        loggedIn:    false,
        me:          {},
    };

    Tracker.autorun(() => {
        if (!Meteor.userId()) {
            auth.initialised = true;
            return;
        }

        const me = Meteor.user() || {};
        if (me._id) {
            auth = assign(auth, { me, loggedIn: true, initialised: true });
        }
    });

    return (
        <Provider store={store}>
            <Router history={history}>
                <div>
                    <Route path="/" store={store} render={() => (<AuthContainer store={store} />)} />
                    <Switch>
                        <Route path="/login" render={() => (<LoginContainer store={store} />)} />
                        <Route path="/sign-up" render={() => (<SignUpContainer store={store} />)} />
                        <Route path="/update-profile" render={() => (<UpdateProfileContainer store={store} />)} />
                        <Route path="/profile" render={() => (<ProfileContainer store={store} />)} />
                    </Switch>
                </div>
            </Router>
        </Provider>
    );
}

const init = () => {
    render(<App />, document.getElementById('App'));
};

export default {
    init,
};
