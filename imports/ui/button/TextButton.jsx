/**
 * @file    Text Button
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import React from 'react';
import {string, bool, func} from 'prop-types';

const TextButton = ({ className, loading, type, text, onClick }) => (
    <button type={type} className={className} onClick={onClick}>
        {loading ? 'loading...' : text}
    </button>
);

TextButton.propTypes = {
    className: string,
    loading:   bool,
    type:      string,
    text:      string.isRequired,
    onClick:   func,
};

TextButton.defaultProps = {
    className: 'c-btn',
    loading:   false,
    type:      'text',
    onClick:   () => {},
};

export default TextButton;
