/**
 * @file    Spinner
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import React from 'react';

const Spinner = () => (
    <div className="lds-ellipsis"><div /><div /><div /><div /></div>
);

export default Spinner;
