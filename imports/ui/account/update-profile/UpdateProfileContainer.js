/**
 * @file    Update Account Container
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import { connect }      from 'react-redux';
import { Meteor }       from 'meteor/meteor';
import {
    getOr, set, assign, curry, compose, get, pick, __,
}                       from 'lodash/fp';
import {
    COMP_NAME as AUTH_COMP_NAME,
}                       from '../auth/AuthContainer';
import UpdateProfile    from './UpdateProfile';

// Constants
// ---------------------------------
export const COMP_NAME = 'UpdateProfileContainer';

// Action
// ---------------------------------
export const UPDATE_PROFILE = {
    REQUEST: 'UPDATE_PROFILE_REQUEST',
    SUCCESS: 'UPDATE_PROFILE_SUCCESS',
    FAILED:  'UPDATE_PROFILE_FAILED',
};

// Reducer
// ---------------------------------
const defaultState = {
    loading: false,
    err:     {},
};

const reducer = (state = defaultState, action) => {
    switch (action.type) {
    case UPDATE_PROFILE.REQUEST:
        return set('loading', true, state);
    case UPDATE_PROFILE.SUCCESS:
        return assign(state, { loading: false, err: {} });
    case UPDATE_PROFILE.FAILED:
        return assign(state, { loading: false, err: action.err });
    default:
        return state;
    }
};

// Connect
// ---------------------------------
const onSubmit = curry((dispatch, newMe) => {
    dispatch({ type: UPDATE_PROFILE.REQUEST });

    Meteor.users.update(
        Meteor.userId(),
        {$set: { profile: newMe.profile }},
        (err) => {
            if (err) {
                dispatch({ type: UPDATE_PROFILE.FAILED, err });
                return;
            }
            dispatch({ type: UPDATE_PROFILE.SUCCESS });
        },
    );
});

const mapStateToProps = (state) => {
    // me, loggedIn from AuthContainer
    const fromLogin  = compose(
        pick(['loggedIn', 'me']),
        get(AUTH_COMP_NAME),
    )(state);

    return compose(
        assign(__, fromLogin),
        getOr(defaultState, COMP_NAME),
    )(state);
};

const mapDispatchToProps = dispatch => ({
    onSubmit: onSubmit(dispatch),
});

const UpdateProfileContainer = connect(mapStateToProps, mapDispatchToProps)(UpdateProfile);

// Export
// ---------------------------------
UpdateProfileContainer.displayName = COMP_NAME;
UpdateProfileContainer.reducer = { [COMP_NAME]: reducer };
UpdateProfileContainer.defaultProps = {
    domID: 'thl-update-account',
};

export default UpdateProfileContainer;
