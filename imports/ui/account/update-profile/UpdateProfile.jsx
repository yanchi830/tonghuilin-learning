/**
 * @file    Update Account view
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import React            from 'react';
import {
    string, func, bool, shape, number,
}                       from 'prop-types';
import {merge, getOr}   from 'lodash/fp';
import printIf          from '../util/printIf';
import queryDOMValue    from '../util/queryDOMValue';
import TextButton       from '../../button/TextButton';
import AccessDenied     from '../AccessDenied';

// Constants
// ----------------------------------------
const text = {
    pageHeading:    'Update profile',
    typeLabel:      'Teacher or Student?',
    surnameLabel:   'Surname',
    givennameLabel: 'Givenname',
};

// Sub Component
// ----------------------------------------
const UpdateProfileForm = ({ domID, loading, onSubmit, me, err }) => {
    const domIDs = {
        form:      `${domID}__form`,
        type:      `${domID}__type`,
        surname:   `${domID}__surname`,
        givenname: `${domID}__givenname`,
    };

    const defaultValue = {
        type:      getOr('', 'profile.type', me),
        surname:   getOr('', 'profile.surname', me),
        givenname: getOr('', 'profile.givenname', me),
    };

    const _onSubmit = (e) => {
        e.preventDefault();

        const newMe = merge(me, {
            profile: {
                type:      queryDOMValue(e.target, domIDs.type),
                surname:   queryDOMValue(e.target, domIDs.surname),
                givenname: queryDOMValue(e.target, domIDs.givenname),
            },
        });

        onSubmit(newMe);
    };

    return (
        <div id={domID} className="thl-auth__form-wrap">
            <h3>{text.pageHeading}</h3>
            <form id={domIDs.form} onSubmit={_onSubmit}>
                <div className="form-group">
                    <label htmlFor={domIDs.type}>{text.typeLabel}</label>
                    <select id={domIDs.type} defaultValue={defaultValue.type} className="form-control">
                        <option value="TEACHER">Teacher</option>
                        <option value="STUDENT">Student</option>
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor={domIDs.surname}>{text.surnameLabel}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={domIDs.surname}
                        defaultValue={defaultValue.surname}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={domIDs.givenname}>{text.givennameLabel}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={domIDs.givenname}
                        defaultValue={defaultValue.givenname}
                    />
                </div>
                <div>
                    <TextButton text="Submit" type="submit" loading={loading} className="btn btn-primary" />
                </div>
            </form>
            {
                printIf(err.message, <p>{JSON.stringify(err)}</p>)
            }
        </div>
    );
};

UpdateProfileForm.propTypes = {
    domID:    string,
    onSubmit: func.isRequired,
    loading:  bool.isRequired,
    err:      shape({
        error:   number,
        message: string,
    }),
    me: shape({
        _id: string,
    }).isRequired,
};

UpdateProfileForm.defaultProps = {
    domID: 'thl-update',
    err:   {},
};


// Default Component
// ----------------------------------------
const UpdateProfile = props =>
    (props.loggedIn ? <UpdateProfileForm {...props} /> : <AccessDenied />);

UpdateProfile.propTypes = {
    loggedIn: bool,
};

UpdateProfile.defaultProps = {
    loggedIn: false,
};

export default UpdateProfile;

