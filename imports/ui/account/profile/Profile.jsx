/**
 * @file    Profile
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import React                    from 'react';
import { string, shape, bool }  from 'prop-types';
import { Redirect }             from 'react-router-dom';

const Profile = ({user, isMe}) =>
    (isMe ? <Redirect to="/update-profile" /> : <div>This is user profile: {JSON.stringify(user)}</div>);

Profile.propTypes = {
    isMe: bool.isRequired,
    user: shape({
        _id: string,
    }).isRequired,
};

export default Profile;
