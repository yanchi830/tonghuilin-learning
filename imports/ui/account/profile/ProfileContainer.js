/**
 * @file    Description
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import { connect }              from 'react-redux';
import { componentWillMount }   from 'react-pure-lifecycle';
import { Tracker }              from 'meteor/tracker';
import { Meteor }               from 'meteor/meteor';
import { getOr, assign, curry } from 'lodash/fp';
import Profile                  from './Profile';

// Constants
// ---------------------------------
export const COMP_NAME = 'ProfileContainer';

export const PROFILE = {
    LOAD: 'PROFILE_LOAD',
};

// Reducer
// ---------------------------------
const defaultState = {
    loading: true,
    user:    {},
    isMe:    false,
};

const reducer = (state = defaultState, action) => {
    switch (action.type) {
    case PROFILE.LOAD:
        return assign(state, action.payload);
    default:
        return state;
    }
};

// Lifecycle
// ---------------------------------
const cwm = ({ loadUser, userId }) => {
    Tracker.autorun(() => {
        loadUser(userId);
    });
};

const ProfileWithLifecycle = componentWillMount(cwm)(Profile);

// Connect to store
// ---------------------------------
const mapStateToProps = getOr(defaultState, COMP_NAME);

/**
 * load given user
 *
 * @param {function}    dispatch
 * @param {object}      theMeteor
 * @param {string}      idFromProps
 *
 * @return {void}
 */
const loadUser = curry((dispatch, theMeteor, idFromProps) => {
    const myId = theMeteor.userId();
    const id = idFromProps || myId;

    const handle = theMeteor.subscribe('user', id);
    const user = theMeteor.users.findOne(id);

    if (user) {
        dispatch({
            type:    PROFILE.LOAD,
            payload: {
                loading: !handle.ready(),
                isMe:    id === myId,
                user,
            },
        });
    }
});

const mapDispatchToProps = dispatch => ({
    loadUser: loadUser(dispatch, Meteor),
});

const ProfileContainer = connect(mapStateToProps, mapDispatchToProps)(ProfileWithLifecycle);

ProfileContainer.displayName = COMP_NAME;
ProfileContainer.reducer = { [COMP_NAME]: reducer };

export default ProfileContainer;

