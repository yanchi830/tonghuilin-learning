/**
 * @file    Description
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import React from 'react';
import { Link } from 'react-router-dom';

export default () => (
    // eslint-disable-next-line jsx-a11y/anchor-is-valid
    <p>You are not allowed to see the page, please <Link to="/login">Login</Link> first.</p>
);
