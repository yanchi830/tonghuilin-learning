import { createElement } from 'react';
import { assert } from 'chai';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Auth from '../Auth';

configure({ adapter: new Adapter() });

const shalComp = props => shallow(createElement(Auth, props));

describe('Account - Auth', () => {
    it('Module should exist, and', () => {
        assert(typeof Auth === 'function');
    });

    it('render "displayName" when "loggedIn: true", and', () => {
        const testProps = {
            loggedIn: true,
            me:       {
                id:      '_foo',
                profile: {
                    displayName: 'Chi Yan',
                },
            },
        };
        const comp = shalComp(testProps);
        assert(comp.find('.thl-auth__name').length === 1, 'Should render displayName');
    });

    it('render "Link" when "loggedIn: false"', () => {
        const testProps = {
            loggedIn: false,
            me:       {
                id:      '_foo',
                profile: {
                    displayName: 'Chi Yan',
                },
            },
        };
        const comp = shalComp(testProps);
        assert(comp.find('.thl-auth__name').length === 0, 'Should not render displayName');
        assert(comp.find('Link').length > 0, 'Should render Links');
    });
});
