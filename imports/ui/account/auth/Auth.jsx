/**
 * @file    Auth component
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import React                    from 'react';
import {string, bool, shape}    from 'prop-types';
import {NavLink}                from 'react-router-dom';

import './scss/global.scss';

// Default Component
// --------------------
const Auth = ({loggedIn, me}) => (loggedIn ?
    <span className="thl-auth__name">Hey, <strong>{me.profile.surname} {me.profile.givenname}</strong></span> :
    <ul className="nav nav-pills nav-fill">
        <li className="nav-item">
            <NavLink className="nav-link" to="/login" activeClassName="active">Login</NavLink>
        </li>
        <li className="nav-item">
            <NavLink className="nav-link" to="/sign-up" activeClassName="active">Sign Up</NavLink>
        </li>
    </ul>);

Auth.propTypes = {
    loggedIn: bool.isRequired,
    me:       shape({
        _id:     string,
        profile: shape({}),
    }).isRequired,
};

export default Auth;
