/**
 * @file    Check current user
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import { connect }              from 'react-redux';
import { Meteor }               from 'meteor/meteor';
import { Tracker }              from 'meteor/tracker';
import { componentDidMount }    from 'react-pure-lifecycle';
import { assign, curry, getOr } from 'lodash/fp';
import Auth                     from './Auth';

import './scss/global.scss';

// Constants
// ---------------------------------
export const COMP_NAME = 'AuthContainer';

// Action
// ---------------------------------
const AUTH = {
    LOAD: 'AUTH_LOAD',
};

// Reducer
// ---------------------------------
const defaultState = {
    initialised: false,
    loggedIn:    false,
    me:          {
        profile: {},
    },
};

const reducer = (state = defaultState, action) => {
    switch (action.type) {
    case AUTH.LOAD:
        return assign(state, action.payload);
    default:
        return state;
    }
};

// Lifecycle
// ---------------------------------
const cdm = curry((Met, Tra, { loadMe }) => {
    Tra.autorun(() => {
        if (!Met.userId()) {
            loadMe({ me: defaultState.me, loggedIn: defaultState.loggedIn, initialised: true });
            return;
        }

        const me = Met.user() || {};
        if (me._id) {
            loadMe({ me, loggedIn: true, initialised: true });
        }
    });
});

const AuthContainerWithLifecycle = componentDidMount(cdm(Meteor, Tracker))(Auth);

// Connect to store
// ---------------------------------
/**
 * Map state to props
 */
const mapStateToProps = getOr(defaultState, COMP_NAME);

/**
 * load/refresh my data
 *
 * @param {function}    dispatch
 * @param {object}      Met             Meteor
 * @param {string}      idFromProps
 *
 * @return {void}
 */
const loadMe = curry((dispatch, payload) => {
    dispatch({ type: AUTH.LOAD, payload});
});

/**
 * Map dispatch to props
 */
const mapDispatchToProps = dispatch => ({
    loadMe: loadMe(dispatch),
});

const AuthContainer = connect(mapStateToProps, mapDispatchToProps)(AuthContainerWithLifecycle);

AuthContainer.displayName = COMP_NAME;
AuthContainer.reducer = { [COMP_NAME]: reducer };

export default AuthContainer;
