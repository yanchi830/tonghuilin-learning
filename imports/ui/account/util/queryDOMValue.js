/**
 * @file    get dom value by its ID
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import getOr from 'lodash/fp/getOr';

// Constants
// ----------------------------------------
const w = typeof window === 'undefined' ? {} : window;

export default (parent = w.document, domId) => {
    if (!parent) {
        return undefined;
    }

    return getOr('', 'value', parent.querySelector(`#${domId}`));
};
