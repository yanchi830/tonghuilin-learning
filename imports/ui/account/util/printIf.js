/**
 * @file    print if value exist
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

const printIf = (valueToCheck, contentToPrint) => (valueToCheck ? contentToPrint : null);

export default printIf;
