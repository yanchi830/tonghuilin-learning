/**
 * @file    Login view
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import React                from 'react';
import { string, bool, number, func, shape } from 'prop-types';
import { Redirect }         from 'react-router-dom';
import printIf              from '../util/printIf';
import queryDOMValue        from '../util/queryDOMValue';
import TextButton           from '../../button/TextButton';
import Spinner              from '../../spinner/Spinner';

import './scss/global.scss';

// Constants
// ----------------------------------------
const text = {
    pageHeading:         'Login',
    usernameLabel:       'Username',
    usernamePlaceholder: 'Email/Username',
    passwordLabel:       'Password',
};

// Sub Component
// ----------------------------------------
const LoginForm = ({ domID, loading, loginWithPassword, err }) => {
    const domIDs = {
        form:     `${domID}__form`,
        username: `${domID}__username`,
        password: `${domID}__password`,
    };

    const _onSubmit = (e) => {
        e.preventDefault();

        loginWithPassword({
            username: queryDOMValue(e.target, domIDs.username),
            password: queryDOMValue(e.target, domIDs.password),
        });
    };

    return (
        <div id={domID} className="thl-auth__form-wrap">
            <h3>{text.pageHeading}</h3>
            <form id={domIDs.form} onSubmit={_onSubmit}>
                <div className="form-group">
                    <label htmlFor={domIDs.username}>{text.usernameLabel}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={domIDs.username}
                        placeholder={text.usernamePlaceholder}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={domIDs.password}>{text.passwordLabel}</label>
                    <input type="password" id={domIDs.password} className="form-control" />
                </div>
                <div>
                    <TextButton text="Submit" type="submit" loading={loading} className="btn btn-primary" />
                </div>
            </form>
            { printIf(err.message, <p>{JSON.stringify(err)}</p>) }
        </div>
    );
};

LoginForm.propTypes = {
    domID:             string.isRequired,
    loading:           bool.isRequired,
    loginWithPassword: func.isRequired,
    err:               shape({
        error:   number,
        message: string,
    }).isRequired,
};

// Default Component
// ----------------------------------------
const Login = (props) => {
    const { loggedIn, initialised } = props;

    if (!initialised) {
        return <Spinner />;
    }

    return (loggedIn ? <Redirect to="/update-profile" /> : <LoginForm {...props} />);
};

Login.propTypes = {
    loggedIn:    bool,
    initialised: bool,
};

Login.defaultProps = {
    loggedIn:    false,
    initialised: false,
};

export default Login;
