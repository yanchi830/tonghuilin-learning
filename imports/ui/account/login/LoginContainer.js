/**
 * @file    LoginContainer
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import { connect }  from 'react-redux';
import { Meteor }   from 'meteor/meteor';
import {
    get, getOr, set, assign, curry, compose, pick,
}                   from 'lodash/fp';
import {
    COMP_NAME as AUTH_COMP_NAME,
}                   from '../auth/AuthContainer';
import Login        from './Login';

// Constants
// ---------------------------------
export const COMP_NAME = 'LoginContainer';


// Action
// ---------------------------------
export const LOGIN_LOGOUT = {
    REQUEST: 'LOGIN_REQUEST',
    SUCCESS: 'LOGIN_SUCCESS',
    FAILED:  'LOGIN_FAILED',
};

// Reducer
// ---------------------------------
const defaultState = {
    loading: false,
    err:     {},
};

const reducer = (state = defaultState, action) => {
    switch (action.type) {
    case LOGIN_LOGOUT.REQUEST:
        return set('loading', true, state);
    case LOGIN_LOGOUT.SUCCESS:
        return assign(state, { loading: false, err: {} });
    case LOGIN_LOGOUT.FAILED:
        return assign(state, { loading: false, err: action.err });
    default:
        return state;
    }
};

// Connect
// ---------------------------------
/**
 * Map state to props
 */
const mapStateToProps = (state) => {
    const fromAuth = compose(
        pick(['loggedIn', 'initialised']),
        get(AUTH_COMP_NAME),
    )(state);

    return compose(
        assign(fromAuth),
        getOr(defaultState, COMP_NAME),
    )(state);
};

/**
 * Callback for Meteor login/logout request
 *
 * @param  {function} dispatch
 *
 * @return {function} err => {}
 */
const requestCallback = dispatch => (err) => {
    if (err) {
        dispatch({ type: LOGIN_LOGOUT.FAILED, err });
        return;
    }
    dispatch({ type: LOGIN_LOGOUT.SUCCESS });
};

/**
 * Meteor login with password
 *
 * @param {function} dispatch
 * @param {object}   Met            Meteor
 * @param {object}   {username, password}
 */
const loginWithPassword = curry((dispatch, Met, {username, password}) => {
    dispatch({ type: LOGIN_LOGOUT.REQUEST });

    Met.loginWithPassword(username, password, requestCallback(dispatch));
});

/**
 * Meteor logout with password
 *
 * @param {function} dispatch
 * @param {object}   Met            Meteor
 *
 * @return {function}
 */
const logoutWithPassword = curry((dispatch, Met) => () => {
    dispatch({ type: LOGIN_LOGOUT.REQUEST });

    Met.logout(requestCallback(dispatch));
});


/**
 * Map dispatch to props
 */
const mapDispatchToProps = dispatch => ({
    loginWithPassword:  loginWithPassword(dispatch, Meteor),
    logoutWithPassword: logoutWithPassword(dispatch, Meteor),
});

const LoginContainer = connect(mapStateToProps, mapDispatchToProps)(Login);

LoginContainer.defaultProps = {
    domID: 'thl-login',
};
LoginContainer.displayName = COMP_NAME;
LoginContainer.reducer = { [COMP_NAME]: reducer };

export default LoginContainer;
