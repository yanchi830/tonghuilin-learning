/**
 * @file    Sign Up Container
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import { connect }  from 'react-redux';
import { Accounts } from 'meteor/accounts-base';
import { getOr, get, set, assign, curry, compose, pick }   from 'lodash/fp';
import { COMP_NAME as AUTH_COMP_NAME } from '../auth/AuthContainer';
import SignUp       from './SignUp';

// Constants
// ---------------------------------
const COMP_NAME = 'SignUpContainer';

// Action
// ---------------------------------
const SIGN_UP = {
    REQUEST: 'SIGN_UP_REQUEST',
    SUCCESS: 'SIGN_UP_SUCCESS',
    FAILED:  'SIGN_UP_FAILED',
};

// Reducer
// ---------------------------------
const defaultState = {
    loading: false,
    err:     {},
};

/**
 * Reducer
 *
 * @param state
 * @param {{ type: {string} }}  action
 * @return {*}
 */
const reducer = (state = defaultState, action) => {
    switch (action.type) {
    case SIGN_UP.REQUEST:
        return set('loading', true, state);
    case SIGN_UP.SUCCESS:
        return assign(state, { loading: false, err: {} });
    case SIGN_UP.FAILED:
        return assign(state, { loading: false, err: action.err });
    default:
        return state;
    }
};

// Connect
// ---------------------------------
/**
 * on submit the registration form
 *
 * @param {function} dispatch
 * @param            form info
 * {{
 *      username:   {string}
 *      password:   {string}
 *      email:      {string}
 *      type:       {string}
 *      surname:    {string}
 *      givenname:  {string}
 * }}
 */
const onSubmit = curry((dispatch, { username, password, email, type, surname, givenname }) => {
    dispatch({ type: SIGN_UP.REQUEST });

    Accounts.createUser({
        username,
        password,
        email,
        profile: {
            type,
            surname,
            givenname,
        },
    }, (err) => {
        if (err) {
            dispatch({ type: SIGN_UP.FAILED, err });
            return;
        }
        dispatch({ type: SIGN_UP.SUCCESS });
    });
});

/**
 * map state to props
 *
 * @param  {object} state
 * @return {*}
 */
const mapStateToProps = (state) => {
    const loginInfo = compose(
        pick(['loggedIn', 'initialised']),
        get(AUTH_COMP_NAME),
    )(state);

    return compose(
        assign(loginInfo),
        getOr(defaultState, COMP_NAME),
    )(state);
};

/**
 * map dispatch to props
 * @param  {function} dispatch
 * @return {{onSubmit: *}}
 */
const mapDispatchToProps = dispatch => ({
    onSubmit: onSubmit(dispatch),
});

const SignUpContainer = connect(mapStateToProps, mapDispatchToProps)(SignUp);

SignUpContainer.displayName = COMP_NAME;
SignUpContainer.reducer = { [COMP_NAME]: reducer };
SignUpContainer.defaultProps = {
    domID: 'thl-sign-up',
};

export default SignUpContainer;
