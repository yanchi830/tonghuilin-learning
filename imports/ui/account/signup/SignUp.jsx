/**
 * @file    Sign Up view
 *
 * @author  Chi Yan <cyan@squiz.net>
 * @date    Apr 2018
 */

import React            from 'react';
import { Redirect }     from 'react-router-dom';
import {
    string, func, bool, shape, number,
}                       from 'prop-types';
import printIf          from '../util/printIf';
import queryDOMValue    from '../util/queryDOMValue';
import TextButton       from '../../button/TextButton';

// Constants
// ----------------------------------------
const text = {
    pageHeading:         'Sign Up',
    usernameLabel:       'Username',
    usernamePlaceholder: 'Email/Username',
    passwordLabel:       'Enter your password',
    passwordRepeatLabel: 'Enter your password again',
    emailLabel:          'Email',
    typeLabel:           'Teacher or Student?',
    surnameLabel:        'Surname',
    givennameLabel:      'Givenname',
};

// Sub Component
// ----------------------------------------
const SignUpForm = ({ domID, loading, onSubmit, err }) => {
    const domIDs = {
        form:           `${domID}__form`,
        username:       `${domID}__username`,
        password:       `${domID}__password`,
        passwordRepeat: `${domID}__password-repeat`,
        email:          `${domID}__email`,
        type:           `${domID}__type`,
        surname:        `${domID}__surname`,
        givenname:      `${domID}__givenname`,
    };

    const _onSubmit = (e) => {
        e.preventDefault();

        onSubmit({
            username:  queryDOMValue(e.target, domIDs.username),
            password:  queryDOMValue(e.target, domIDs.password),
            email:     queryDOMValue(e.target, domIDs.email),
            type:      queryDOMValue(e.target, domIDs.type),
            surname:   queryDOMValue(e.target, domIDs.surname),
            givenname: queryDOMValue(e.target, domIDs.givenname),
        });
    };

    return (
        <div id={domID} className="thl-auth__form-wrap">
            <h3>{text.pageHeading}</h3>
            <form id={domIDs.form} onSubmit={_onSubmit}>
                <div className="form-group">
                    <label htmlFor={domIDs.username}>{text.usernameLabel}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={domIDs.username}
                        placeholder={text.usernamePlaceholder}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={domIDs.password}>{text.passwordLabel}</label>
                    <input type="password" id={domIDs.password} className="form-control" />
                </div>
                <div className="form-group">
                    <label htmlFor={domIDs.passwordRepeat}>{text.passwordRepeatLabel}</label>
                    <input type="password" id={domIDs.passwordRepeat} className="form-control" />
                </div>
                <div className="form-group">
                    <label htmlFor={domIDs.type}>{text.typeLabel}</label>
                    <select id={domIDs.type} className="form-control">
                        <option value="TEACHER">Teacher</option>
                        <option value="STUDENT">Student</option>
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor={domIDs.email}>{text.emailLabel}</label>
                    <input type="text" id={domIDs.email} className="form-control" />
                </div>
                <div className="form-group">
                    <label htmlFor={domIDs.surname}>{text.surnameLabel}</label>
                    <input type="text" id={domIDs.surname} className="form-control" />
                </div>
                <div className="form-group">
                    <label htmlFor={domIDs.givenname}>{text.givennameLabel}</label>
                    <input type="text" id={domIDs.givenname} className="form-control" />
                </div>
                <div>
                    <TextButton text="Submit" type="submit" loading={loading} className="btn btn-primary" />
                </div>
            </form>
            {
                printIf(err.message, <p>{JSON.stringify(err)}</p>)
            }
        </div>
    );
};

SignUpForm.propTypes = {
    domID:    string.isRequired,
    onSubmit: func.isRequired,
    loading:  bool.isRequired,
    err:      shape({
        error:   number,
        message: string,
    }).isRequired,
};

// Default Component
// ----------------------------------------
const SignUp = props =>
    (props.loggedIn ? <Redirect to="/update-profile" /> : <SignUpForm {...props} />);

SignUp.propTypes = {
    loggedIn: bool,
    store:    shape({
        dispatch: func,
    }),
};

SignUp.defaultProps = {
    loggedIn: false,
    store:    {},
};

export default SignUp;
